﻿import java.util.Scanner;

 

class Fahrkartenautomat {
    public static void main(String[] args) {
        Scanner tastatur = new Scanner(System.in);

 

        double zuZahlenderBetrag;
        double eingezahlterGesamtbetrag;
        double eingeworfeneMünze;
        double rückgabebetrag;
        int anzahlTicket;

 

        zuZahlenderBetrag = farkartenBestellungErfassen();

 

        eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);

 

        fahrkartenAusgeben();

 

        rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
        
        
        
    }

 

    
    
    public static double farkartenBestellungErfassen() {
        Scanner tastatur = new Scanner(System.in);
        

 

        System.out.print("Anzahl der Tickets: ");    
        int anzahlTicket = tastatur.nextInt();
        
        if (anzahlTicket < 1 || anzahlTicket > 10 ) {
            
            System.out.println("Sie können nur 1 bis 10 Tickets kaufen");
            System.out.println("Die Anzahl der Tickets ist jetzt: 1");
            System.out.println("");
            anzahlTicket = 1;
        }

 

        
        System.out.print("Zu zahlender Betrag (EURO): ");
        double zuZahlenderBetrag = tastatur.nextDouble();
        
        
        zuZahlenderBetrag = zuZahlenderBetrag * anzahlTicket;
        return zuZahlenderBetrag;
    }

 

    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
        Scanner tastatur = new Scanner(System.in);

 

        double eingezahlterGesamtbetrag = 0.0;
        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
            double ausstehendeBetrage = zuZahlenderBetrag;
            ausstehendeBetrage = ausstehendeBetrage - eingezahlterGesamtbetrag;
            System.out.printf("Noch zu zahlen: %.2f €\n", ausstehendeBetrage);
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            double eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }

 

        return eingezahlterGesamtbetrag;
    }

 

    public static void fahrkartenAusgeben() {
        Scanner tastatur = new Scanner(System.in);

 

        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }

 

    
    public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
        Scanner tastatur = new Scanner(System.in);

 

        double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if (rückgabebetrag > 0.0) {
            System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
            System.out.println("wird in folgenden Münzen ausgezahlt:");

 

            while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
                System.out.println("2 EURO");
                rückgabebetrag -= 2.0;
            }
            while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
                System.out.println("1 EURO");
                rückgabebetrag -= 1.0;
            }
            while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
                System.out.println("50 CENT");
                rückgabebetrag -= 0.5;
            }
            while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
                System.out.println("20 CENT");
                rückgabebetrag -= 0.2;
            }
            while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
                System.out.println("10 CENT");
                rückgabebetrag -= 0.1;
            }
            while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
                System.out.println("5 CENT");
                rückgabebetrag -= 0.05;
            }
            while (rückgabebetrag >= 0.02)// 2 CENT-Münzen
            {
                System.out.println("5 CENT");
                rückgabebetrag -= 0.02;
            }
            while (rückgabebetrag >= 0.01)// 1 CENT-Münzen
            {
                System.out.println("5 CENT");
                rückgabebetrag -= 0.01;
            }
        }

 

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
                + "Wir wünschen Ihnen eine gute Fahrt.");

 

        
    }
    
    
    
    
}